import { Router } from "express";
import log from "../logger";
import UserService from "../services/user-service";
const userService = new UserService();

const router = Router();

router.get("/", async (req, res, next) => {
  try {
    const users = await userService.findAll();
    res.status(200).json(users);
  } catch (error) {
    log.error(error);
    res.sendStatus(404);
  }
});

router.get("/ping", async (req, res, next) => {
  res.send("pong");
});

export default router;
