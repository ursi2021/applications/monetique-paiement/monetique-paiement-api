import { Router } from "express";
import log from "../logger";

const router = Router();

router.get("", async (req, res, next) => {
  try {
    const jobs = [
      {
        j: 0, //index for day of the week. Monday at 1, Sunday at 7, 0 is everyday, null for specific date
        h: 12, // required, must be valid
        m: 0, // required, must be valid
        date: null, // Date format if j != null, else  null.
        name: "Every day at noon", // required
        route: "localhost", // required
      },
    ];
    res.status(200).json(jobs);
  } catch (error) {
    log.error(error);
    res.sendStatus(404);
  }
});

export default router;
