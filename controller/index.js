import { Router } from "express";
import axios from "axios";
import log from "../logger";

const router = Router();

/**@swagger
 * /monetique-paiement/hello:
 *   get:
 *     summary: GET Hello World
 *     tags:
 *        - Hello World
 *     responses:
 *       200:
 *         description: Send Hello World in JSON as response
 */
router.get("/monetique-paiement/hello", (req, res, next) => {
  res.json({ "monetique-paiement": "Hello World !" });
});

/**@swagger
 * /monetique-paiement/hello/all:
 *   get:
 *     summary: GET Hello World on all application
 *     tags:
 *        - Hello World
 *     responses:
 *       200:
 *         description: Send Hello World in JSON as response for all applications
 */
router.get("/monetique-paiement/hello/all", async (req, res, next) => {
  try {
    const urls = [
      { name: "Relation client", url: process.env.URL_RELATION_CLIENT },
      { name: "Decisionnel", url: process.env.URL_DECISIONNEL },
      { name: "Gestion promotion", url: process.env.URL_GESTION_PROMOTION },
      {
        name: "Referenciel produit ",
        url: process.env.URL_REFERENTIEL_PRODUIT,
      },
      { name: "Caisse", url: process.env.URL_CAISSE },
      { name: "Gestion Commercial", url: process.env.URL_GESTION_COMMERCIALE },
      { name: "Back Office", url: process.env.URL_BACK_OFFICE_MAGASIN },
      { name: "E Commerce", url: process.env.URL_E_COMMERCE },
      { name: "Entrepots", url: process.env.URL_ENTREPROTS },
    ];
    const ret = await Promise.all(
      urls.map(async (url) => {
        try {
          const response = await axios.get(url.url + "/hello");
          const ret = { ...url, data: response.data, error: false };
          return ret;
        } catch (err) {
          return { ...url, data: null, error: err };
        }
      })
    );
    res.render("applications-status", { apps: ret });
  } catch (error) {
    log.error(error);
    res.sendStatus(404);
  }
});

/**@swagger
 * /monetique-paiement/clients:
 *   get:
 *     tags:
 *       - Clients
 *     summary: GET clients
 *     responses:
 *       200:
 *         description: Send Client in HTML Format
 */
router.get("/monetique-paiement/clients", async (req, res, next) => {
  // const test = [
  //   {
  //     Nom: "John",
  //     Prenom: "Doe",
  //     Credit: 42.42,
  //     Paiement: 42,
  //     Compte: "Compte",
  //   },
  // ];
  try {
    const ret = await axios.get(process.env.URL_RELATION_CLIENT + '/clients');
    res.render("clients", { clients: ret.data });
  } catch (error) {
    log.error(error);
    res.status(404).send(error);
  }
});

export default router;
