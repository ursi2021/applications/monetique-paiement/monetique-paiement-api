import { Router } from "express";
import log from "../logger";
import BankDetailsService from "../services/bank-details-service";
const bankDetailsService = new BankDetailsService();

const router = Router();

// POST /bank-details
router.post("/", async (req, res) => {
  //log.info(req);
  const random = Math.random();
  if (random < 0.5) res.status(200).json({ accepted: true });
  else res.status(200).json({ accepted: false });
});

export default router;
