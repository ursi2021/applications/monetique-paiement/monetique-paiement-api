import { Router } from "express";
import axios from "axios";

import paymentService from "../services/payment-service";
import log from "../logger";

const router = Router();

router.get("/view", async (req, res, next) => {
  res.render("payments", {});
});

router.get("/view/caisse", async (req, res, next) => {
  // const test = {
  //   id: 4242,
  //   amount: 456,
  //   "card-number": "4242 4242 4242 4242",
  //   "validity-date": "09/21",
  //   ccv: 456,
  //   "last-name": "DOE",
  //   "first-name": "JOHN",
  //   provider: "MASTERCARD",
  // };
  // res.render("payment-caisse", { payment: test });

  try {
    const ret = await axios.get(process.env.URL_CAISSE + "/payment");
    res.render("payment-caisse", { payment: ret.data });
  } catch (error) {
    log.error(error);
    res.sendStatus(404);
  }
});

router.get("/view/e-commerce", async (req, res, next) => {
  // const test = {
  //   id: 4242,
  //   amount: 456,
  //   "card-number": "4242 4242 4242 4242",
  //   "validity-date": "09/21",
  //   ccv: 456,
  //   "last-name": "DOE",
  //   "first-name": "JOHN",
  //   provider: "MASTERCARD",
  // };
  // res.render("payment-e-commerce", { payment: test });

  try {
    const ret = await axios.get(process.env.URL_E_COMMERCE + "/payment");
    res.render("payment-e-commerce", { payment: ret.data });
  } catch (error) {
    log.error(error);
    res.sendStatus(404);
  }
});

router.get("/view/random", async (req, res, next) => {
  const { application } = req.query;
  let url = "";
  let status = "";

  if (application === "caisse") {
    url = process.env.URL_CAISSE + "/payment";
  }

  if (application === "e-commerce") {
    url = process.env.URL_E_COMMERCE + "/payment";
  }

  const random = Math.random();
  if (random < 0.5) {
    status = "accepted";
  } else {
    status = "declined";
  }

  // const data = {
  //   id: 424242,
  //   amount: 456,
  //   "card-number": "4242 4242 4242 4242",
  //   "validity-date": "09/21",
  //   ccv: 456,
  //   "last-name": "DOE",
  //   "first-name": "JOHN",
  //   provider: "MASTERCARD",
  // };

  try {
    const { data } = await axios.get(url);
    const ret = await paymentService.create(data.id, status, application);
    if (!ret) {
      res.json(null);
    } else {
      res.status(200).json({
        "payment-id": ret.paymentId, // TODO: camelCase on next sprint
        status: ret.status,
        application: ret.application,
      });
    }
  } catch (error) {
    log.error(error);
    res.sendStatus(404);
  }
});

router.get("/api", async (req, res, next) => {
  try {
    const { paymentId, status, application } = req.query;
    const ret = await paymentService.create(paymentId, status, application);
    res.status(200).json(ret);
  } catch (error) {
    log.error(error);
    res.sendStatus(404);
  }
});

router.get("/", async (req, res, next) => {
  try {
    const payments = await paymentService.findAll();
    res.status(200).json(payments);
  } catch (error) {
    log.error(error);
    res.sendStatus(404);
  }
});

router.get("/:paymentId", async (req, res, next) => {
  try {
    const { paymentId } = req.params;
    const payment = await paymentService.find(paymentId);
    if (!payment) {
      res.json(null);
    } else {
      res.status(200).json({
        id: payment.id,
        status: payment.status,
      });
    }
  } catch (error) {
    log.error(error);
    res.status(404).json({ error: error.message });
  }
});

router.post("/", async (req, res, next) => {
  try {
    let status = "";
    const { from: application } = req.body;
    const random = Math.random();
    if (random < 0.5) {
      status = "accepted";
    } else {
      status = "declined";
    }
    const { id } = await paymentService.createWithStatus(application, status);
    log.info(`[Payment] ${id}: ${status}`);
    res.status(200).json({
      id,
      status,
    });

    // Account from relation client
    const { account } = req.body;
    if (account != null) {
      const { data } = await axios.get(
        `${process.env.URL_RELATION_CLIENT}/relation-client/client-account/${account}`
      );
      log.info(
        `[Payment]: Called relation-client for account: ${account}, received ${data.account["card-number"]}`
      );
    } else {
      log.info(`[Payment] ${id}: without card`);
    }
  } catch (error) {
    log.error(error);
    res.sendStatus(404);
  }
});

export default router;
