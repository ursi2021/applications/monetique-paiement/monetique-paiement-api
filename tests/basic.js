import chai from 'chai';
import { equal } from 'assert';

import app from "../app";

chai.use(require('chai-http'));
const { expect, request } = chai;

describe('Array', function () {
  describe('#indexOf()', function () {
    it('should return -1 when the value is not present', function () {
      equal([1, 2, 3].indexOf(4), -1);
    });
  });
});

describe("Server!", () => {
  it("welcomes user to the api", done => {
    request(app)
      .get("/")
      .end((err, res) => {
        expect(res).to.have.status(404);
        done();
      });
  })});