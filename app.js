import env from "./config/environment";
import express, { json, urlencoded } from "express";
import cookieParser from "cookie-parser";
import swaggerJSDoc from "swagger-jsdoc";
import { serve, setup } from "swagger-ui-express";
import createError from "http-errors";
import morgan from "morgan";
import path from "path";

import log from "./logger";
import options from "./logger/logger-options";
import indexRouter from "./controller";
import usersRouter from "./controller/users";
import bankDetailsRouter from "./controller/bank-details";
import paymentsRouter from "./controller/payment";
import clockRouter from "./controller/clock";

import registerToKong from "./services/kong-service";
registerToKong();

const app = express();
const swaggerSpec = swaggerJSDoc(options);

/** Middleware */
app.set("views", path.join(__dirname, "views"));
app.set("view engine", "ejs");

app.use(json());
app.use(urlencoded({ extended: false }));
app.use(cookieParser());
app.enable("trust proxy");
app.use(morgan("combined"));
app.use(express.static(path.join(__dirname, "public")));

/** Controller */
app.use("/api-docs", serve, setup(swaggerSpec));
app.use("/", indexRouter);
app.use("/users", usersRouter);
app.use("/clock-register", clockRouter);
app.use("/bank-details", bankDetailsRouter);
app.use("/monetique-paiement/payment", paymentsRouter);

/** Unhandled Requests */
app.use((_req, res, next) => {
  next(createError(404));
});
app.use((err, req, res, next) => {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get("env") === "development" ? err : {};
  log.error(err);
  res.sendStatus(err.status || 500);
});

const port = Number(process.env.PORT || 3000);
app.listen(port, () => {
  log.info(`Server listening on ${port}`);
});

export default app;
