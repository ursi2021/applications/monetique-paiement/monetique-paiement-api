import { Model, DataTypes } from "sequelize";
import sequelize from "./sequelize";

/**
 * @swagger
 * definitions:
 *  User:
 *      type: object
 *      properties:
 *          id:
 *              type: string
 *          status:
 *              type: string
 *          appplication:
 *              type: string
 *          validityDate:
 *              type: string
 *          ccv:
 *              type: string
 *          provider:
 *              type: string
 */

class Payment extends Model { }
Payment.init(
    {
        id: {
            primaryKey: true,
            type: DataTypes.UUID,
            defaultValue: DataTypes.UUIDV4,
        },
        paymentId: {
            type: DataTypes.INTEGER,
            allowNull: true,
        },
        status: {
            type: DataTypes.STRING,
            allowNull: true,
        },
        application: {
            type: DataTypes.STRING,
            allowNull: false,
        },
    },
    {
        sequelize,
        modelName: "payment"
    }
);

export default Payment;