import { Model, DataTypes } from "sequelize";
import sequelize from "./sequelize";

/**
 * @swagger
 * definitions:
 *  User:
 *      type: object
 *      properties:
 *          firstName:
 *              type: string
 *          lastName:
 *              type: string
 *          cardNumber:
 *              type: string
 *          validityDate:
 *              type: string
 *          ccv:
 *              type: string
 *          provider:
 *              type: string
 */

class BankDetails extends Model { }
BankDetails.init(
    {
        firstName: {
            type: DataTypes.STRING,
            allowNull: false,
        },
        lastName: {
            type: DataTypes.STRING,
            allowNull: false,
        },
        cardNumber: {
            type: DataTypes.STRING,
            allowNull: false,
        },
        validityDate: {
            type: DataTypes.DATE,
            allowNull: false,
        },
        ccv: {
            type: DataTypes.STRING,
            allowNull: false,
        },
        provider: {
            type: DataTypes.STRING,
            allowNull: false,
        }
    },
    {
        sequelize,
        modelName: "bank_details"
    }
);

export default BankDetails;