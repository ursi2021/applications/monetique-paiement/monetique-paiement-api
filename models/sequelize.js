import { Sequelize } from "sequelize";
import log from "../logger";

const sequelize = new Sequelize(
  process.env.URSI_DB_NAME || "monetique-paiement",
  process.env.URSI_DB_USER || "root",
  process.env.URSI_DB_PASSWORD || "azerty",
  {
    host: process.env.URSI_DB_HOST || 'localhost',
    port: Number(process.env.URSI_DB_PORT || '3306'),
    logging: (msg) => log.verbose(msg),
    dialectOptions: {
      timezone: "Etc/GMT0",
    },
    dialect: "mariadb",
  }
);

(async () => {
  try {
    await sequelize.authenticate();
    await sequelize.sync();
    log.info("Connection has been established successfully.");
  } catch (error) {
    log.error("Unable to connect to the database:", error);
  }
})();


export default sequelize;
