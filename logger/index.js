import { createLogger, format as _format, transports as _transports } from 'winston';
import dateFormat from 'dateformat';

// logger setup
const logger = createLogger(
    {
        level: 'silly',
        format: _format.combine(
            _format.colorize(),
            _format.timestamp(),
            _format.printf(info => dateFormat(info.timestamp,"yyyy-mm-dd h:MM:ss") + ` [${info.level}]: ${info.message}`),
        ),
        transports: [
            new _transports.Console()
        ]
    })

export default logger