# Monétique-Paiement

## API

### Make payment

```
# Request
POST /monetique-paiement/payment
{
	"from": "caisse" or "e-commerce",
	"amount": 100
}

# Response
{
  "id": "auto-generated-uuidv4",
  "status": "accepted" or "declined"
}
```

### Get payment

```
# Request
GET /monetique-paiement/:paymentId

# Response
{
  "id": "auto-generated-uuidv4",
  "status": "accepted" or "declined"
}
```

## Getting Started

### Prerequisites

To build and run this app locally you will need a few things:

- [node](https://nodejs.org/en/) LTS Version

### Installing

To install the project follow those steps :

- Clone the repository

- Go in the repository

```bash
cd [repository_name]
```

- Install node modules (please check you are inside the repository before installing the modules)

```bash
npm install
```

### Run Application (Development Mode with Hot Reload)

```bash
npm run dev
```

### Run Application (Production Mode)

```bash
npm start
```

### Configure

Put your local variables in **/config/.env.local**

| Key         | Description                                           |
| ----------- | ----------------------------------------------------- |
| DB_PORT     | Database port                                         |
| DB_NAME     | Database name                                         |
| DB_HOST     | Database Uri                                          |
| DB_USER     | Database user (with write/read privileges on DB_NAME) |
| DB_PASSWORD | Database password                                     |

## Commit

### Format

```bash
type($scope): $message

$body
```

### Type

Must be one of the following:

| Key          | Description                                                   |
| ------------ | ------------------------------------------------------------- |
| **build**    | Changes that affect the build system or external dependencies |
| **ci**       | Changes to our CI configuration files and scripts             |
| **docs**     | Documentation only changes                                    |
| **feat**     | A new feature                                                 |
| **fix**      | A bug fix                                                     |
| **perf**     | A code change that improves performance                       |
| **refactor** | A code change that neither fixes a bug nor adds a feature     |
| **style**    | Changes that do not affect the meaning of the code            |
| **test**     | Adding missing tests or correcting existing tests             |

### Example

```bash
fix(release): need to depend on latest rxjs and zone.js

The version in our package.json gets copied to the one we publish, and users need the latest of these.
```

## Branch

### Format

```bash
(feature|fix|hotfix|release)/$scope
```

### Type

Must be one of the following:

| Key          | Description                                                   |
| ------------ | ------------------------------------------------------------- |
| **build**    | Changes that affect the build system or external dependencies |
| **ci**       | Changes to our CI configuration files and scripts             |
| **docs**     | Documentation only changes                                    |
| **feat**     | A new feature                                                 |
| **fix**      | A bug fix                                                     |
| **perf**     | A code change that improves performance                       |
| **refactor** | A code change that neither fixes a bug nor adds a feature     |
| **style**    | Changes that do not affect the meaning of the code            |
| **test**     | Adding missing tests or correcting existing tests             |

## Run Tests

```bash
npm test
```

## Run the code coverage with yours tests

To start the coverage the application :

```bash
npm run coverage
```
## Database with Docker

```bash
# Will launch a mariadb container in detached mode.
# More information inside `docker-compose.yml` file
$ docker-compose up -d 
```
