import axios from "axios";
import log from "../logger";

const sleep = (ms) => {
  return new Promise((resolve) => {
    setTimeout(resolve, ms);
  });
};

const register_kong = async () => {
  try {
    await axios.post("http://kong:8081/services/", {
      name: process.env.APP_NAME,
      url: "http://" + process.env.APP_NAME,
    });

    await axios.post(
      "http://kong:8081/services/" + process.env.APP_NAME + "/routes",
      {
        paths: ["/" + process.env.APP_NAME],
        name: process.env.APP_NAME,
      }
    );

    log.info(`[Kong] ${process.env.APP_NAME} registered to Kong`);
    return true;
  } catch (error) {
    log.error("[Kong] failed connection");
    return false;
  }
};

const isAlreadyRegistered = async () => {
  try {
    const { data } = await axios.get(
      `http://kong:8081/services/${process.env.APP_NAME}`
    );
    log.info(`[Kong] ${process.env.APP_NAME} was previously registered`);
    return data.name === process.env.APP_NAME;
  } catch (error) {
    log.warn(`[Kong] ${process.env.APP_NAME} was not previously registered`);
    return false;
  }
};

const registerToKong = async () => {
  try {
    let registered = await isAlreadyRegistered();
    const waitingTime = 1000;
    while (!registered) {
      registered = await register_kong();
      if (!registered) {
        log.warn(`[Kong] Waiting ${waitingTime}ms before retrying`);
        await sleep(1000);
      }
    }
  } catch (error) {
    log.error("[Kong] failed connection", error);
  }
};

export default registerToKong;
