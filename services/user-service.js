import model from "../models/user";

class UserService {
  constructor() {
    this.users = model;
  }

  findAll() {
    return this.users.findAll({});
  }
}

export default UserService;

