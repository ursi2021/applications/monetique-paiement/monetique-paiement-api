import Payment from "../models/payment-model";

class PaymentService {
  constructor() {
    this.payments = Payment;
  }

  createOne(application) {
    if (application == null) {
      throw new Error("Bad request body, Missing application name");
    }
    return this.payments.create({
      application,
    });
  }

  createWithStatus(application, status) {
    if (application == null || status == null) {
      throw new Error("Bad request body, Missing application name or status");
    }
    return this.payments.create({
      application,
      status,
    });
  }

  updateStatus(id, status) {
    if (id == null) {
      throw new Error("Bad request body, Missing payment id");
    }
    return this.payments.update(
      {
        status,
      },
      {
        where: {
          id,
        },
      }
    );
  }

  create(paymentId, status, application) {
    if (paymentId == null || status == null || application == null) {
      throw new Error("Bad request body");
    }
    return this.payments.create({
      paymentId,
      status,
      application,
    });
  }

  findAll() {
    return this.payments.findAll({});
  }

  findOne(paymentId, application) {
    return this.payments.findOne({
      where: {
        paymentId,
        application,
      },
    });
  }

  find(id) {
    return this.payments.findOne({
      where: {
        id,
      },
    });
  }
}

export default new PaymentService();
